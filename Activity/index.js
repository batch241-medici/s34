const express = require("express");
const app = express();
const port = 4000;

app.use(express.json())
app.use(express.urlencoded({extended: true}));

app.get("/home", (request, response) => {
	response.send("Welcome to home page.")
})

let users = [
{
	"username": "johndoe",
	"password": "johndoe1234"
}
];

app.get("/users", (request, response) => {
	response.send(users);
	users.push(request.body);
});

app.delete("/delete-user", (request, response) => {
	response.send(`User ${request.body.username} has been deleted.`);
})

app.listen(port, () => console.log(`Server running at port ${port}`));